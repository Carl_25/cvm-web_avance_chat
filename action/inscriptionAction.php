<?php
	require_once("nusoap-0.9.5/lib/nusoap.php");
	require_once("action/CommonAction.php");

	class InscriptionAction extends CommonAction {
		public $erreur;
		public $key;

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {

			if (isset($_POST["matricule"])) {

				$matricule = $_POST["matricule"];
				$prenom = $_POST["prenom"];
				$nom = $_POST["nom"];
				$nomUsager = $_POST["nomUsager"];
				$mdp = $_POST["mdp"];
				$mdp = md5($mdp);
				$bienvenue = $_POST["bienvenue"];

				$soapClient = new nusoap_client('http://apps-de-cours.com/web-chat/server/services.php', false);
				$this->erreur = $soapClient->getError();
			
				if (empty($this->erreur )) {

					$this->key = $soapClient->call('enregistrer', array('matricule' => $matricule ,
					'prenom' => $prenom,
					'nom' => $nom,
					'nomUsager' => $nomUsager,
					'motDePasse' => $mdp,
					'texteBienvenue' => $bienvenue) );
					
					
				}

			}			
		}
	}