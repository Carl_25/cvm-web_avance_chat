<?php
	require_once("action/CommonAction.php");

	class AjaxAction extends CommonAction {
		public $resultat;
		public $envoieMessage;
	
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);			
		}
	
		protected function executeAction() {

			if (!empty($_POST["listeMembres"])) {
				$this->resultat = $this->soapClient->call('listeDesMembres', array('clef' => $_SESSION["cle"] ));
			}
			else if (!empty($_POST["champMessage"])){
				$message = $_POST["champMessage"];
				$this->resultat = $message;

				$this->soapClient->call('ecrireMessage', array('clef' => $_SESSION["cle"], 'message' => $message ));

			}
			else if (!empty($_POST["tousLesMessages"])){
				$this->resultat = $this->soapClient->call('lireMessages', array('clef' => $_SESSION["cle"] ));
			}

		}
	}