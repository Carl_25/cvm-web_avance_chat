<?php 
	require_once("nusoap-0.9.5/lib/nusoap.php");	
	session_start();

	abstract class CommonAction {
		public static $VISIBILITY_PUBLIC = 0;
		public static $VISIBILITY_MEMBER = 1;
		public static $VISIBILITY_MODERATOR = 2;
		public static $VISIBILITY_ADMINISTRATOR = 3;
		public $soapClient;



		public function __construct($pageVisibility) {
			$this->pageVisibility = $pageVisibility;
		}

		public function execute() {

			$this->soapClient = new nusoap_client('http://apps-de-cours.com/web-chat/server/services.php', false);
			
			$this->erreur = $this->soapClient->getError();
			

			if (empty($this->erreur )) {

				//se déconnecter du chat
				if (!empty($_GET["disconnect"]) ){

					$this->key = $this->soapClient->call('deconnecter', array('clef' => $_SESSION["cle"] ));

					session_unset();
					session_destroy();
					session_start();

					header('location:index.php');
					exit;
				//se désabonner du chat
				}
				else if (!empty($_GET["desabonne"])){
					$this->key = $this->soapClient->call('desenregistrer', array('clef' => $_SESSION["cle"] ));
					header('location:index.php');
					exit;
				}

			}

			$this->executeAction();

		}

		protected abstract function executeAction();
	}

