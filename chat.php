<?php  
	require_once("action/ChatAction.php");
	$action = new ChatAction();
	$action->execute();
	
?>

<script>
	function loadjscssfile(filename, filetype){
		 if (filetype=="js"){ //if filename is a external JavaScript file
		  var fileref=document.createElement('script');
		  fileref.setAttribute("type","text/javascript");
		  fileref.setAttribute("src", filename);
		 }
		 else if (filetype=="css"){ //if filename is an external CSS file
		  var fileref=document.createElement("link");
		  fileref.setAttribute("rel", "stylesheet");
		  fileref.setAttribute("type", "text/css");
		  fileref.setAttribute("href", filename);
		 }
		 if (typeof fileref!="undefined"){
		 	document.getElementsByTagName("head")[0].appendChild(fileref);
		 }
		  
	}

	loadjscssfile("css/global.css", "css");
	loadjscssfile("css/jeuContra.css", "css");
	loadjscssfile("js/javascript.js", "js");
	loadjscssfile("js/Bonhomme.js", "js");
	loadjscssfile("js/Boss.js", "js");
	loadjscssfile("js/Missile.js", "js");
	loadjscssfile("js/MissileEnnemi.js", "js");
</script>

<!DOCTYPE html>
<html>
<head>

	<meta charset=utf-8>
	<script src="js/jquery.js"></script>
	<script src="js/javascriptChat.js"></script>
	<title>Chat</title>
</head>
<body>
		
		<div class="jeuArea" id="jeuArea"></div>

		<div class="fin" id="fin"></div>
		<div class="win" id="win"></div>

		<div>
			<div class="colonne2">	
				<div>
					<ul class="listeUsers" id="listeUsers">
						
					</ul>
				</div>
				
				<input type="button" value="envoyer" onclick="envoyerMessage()"/>
				
			</div>


			<div class="fenetreChat">
				<ul class="tousLesMessages" id="tousLesMessages">
				    
				</ul>

			</div>
			<input id="champMessage" class="positionInputText" type="text" name="champMessage">
		</div>

		<div class="clearboth"></div>


</body>
<footer>

	<a href="?desabonne=true">se désabonner</a>
	<a id="disconnect" href="?disconnect=true">déconnexion</a>

</footer>

</html>