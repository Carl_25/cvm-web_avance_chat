<?php  
	require_once("action/ChatAction.php");
	$action = new ChatAction();
	$action->execute();
	
?>

<script>
	function loadjscssfile(filename, filetype){
		 if (filetype=="js"){ //if filename is a external JavaScript file
		  var fileref=document.createElement('script');
		  fileref.setAttribute("type","text/javascript");
		  fileref.setAttribute("src", filename);
		 }
		 else if (filetype=="css"){ //if filename is an external CSS file
		  var fileref=document.createElement("link");
		  fileref.setAttribute("rel", "stylesheet");
		  fileref.setAttribute("type", "text/css");
		  fileref.setAttribute("href", filename);
		 }
		 if (typeof fileref!="undefined"){
		 	document.getElementsByTagName("head")[0].appendChild(fileref);
		 }
		  
	}

	loadjscssfile("css/global.css", "css");
	loadjscssfile("css/secretOfMana.css", "css");

	loadjscssfile("js/jquery.js", "js");
	loadjscssfile("js/javascriptChat.js", "js");

	loadjscssfile("js/secretOfMana/javascript.js", "js");



</script>

<!DOCTYPE html>
<html>
<head>

	<meta charset=utf-8>
	<title>Chat</title>
</head>
<body>
		<a class="floatright" href="?disconnect=true">déconnexion</a>
		<div class="container">
			<div class="range1">
				<div class="fenetreChat">
					<ul class="tousLesMessages" id="tousLesMessages">
				    
					</ul>
				</div>

				<div class="users">
					<ul class="listeUsers" id="listeUsers">
						
					</ul>
				</div>
			<div class="clearboth"></div>
				
			</div>
			<div class="range2">
				<input id="champMessage" class="inputText" type="text" name="champMessage">
				<input id="boutonMessage" type="button" value="envoyer" onclick="envoyerMessage()"/>
			</div>
			
			
		</div>
		<div class="clearboth"></div>

		<div class="animation">
			
		</div>

</body>
<footer><a href="?desabonne=true">se désabonner</a></footer>

</html>