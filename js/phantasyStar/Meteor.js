function Meteor(){
	this.x= Math.random() * 1000;
	this.y= Math.random() * 0;

	this.node = document.createElement("div");
	this.node.setAttribute("class", "meteor");

	this.node.style.left=this.x + "px";
	this.node.style.top=this.y + "px";


	document.body.appendChild(this.node);
	
	this.velocityY = 0 ;
	this.velocityFactorY = -0.1;
}

Meteor.prototype.tick = function() {

		this.velocityY += this.velocityFactorY;
		this.y -= this.velocityY;
		this.x -= this.velocityY;
		this.node.style.top=this.y + "px";
		this.node.style.left=this.x + "px";

		var alive = this.y < 850 && this.x < 1000;

		if(!alive){
			this.node.parentNode.removeChild(this.node);
		}

		return this.y < 850 && this.x < 1000;
	}