$( document ).ready( init );

function init(){
	setTimeout(getListeMembres,1500);
	setTimeout(getMessages,1500);


}

function getListeMembres(){
	$.ajax({
		type: 'POST', 					
		url: 'ajax.php',
		data: {				
			//nom du post: "valeur",
			listeMembres : "listeMembres"
		}
	})
	.done(function(data) {

		data = JSON.parse(data); 

		/*var str = "<";
		data = str.replace("<", "&lt");
		console.log(data);*/

		afficherMembres(data);	
		
		setTimeout(getListeMembres, 1500);
	})

}
function getMessages(){
	$.ajax({
		type: 'POST', 					
		url: 'ajax.php',
		data: {				
			//nom du post: "valeur",
			tousLesMessages : "tousLesMessages"
		}
	})
	.done(function(data) {

		data = JSON.parse(data); 

		/*for (var i=0;i<data.length;i++){
			console.log(data[i].message );
		}*/

		afficherMessageDesAutres(data);

		setTimeout(getMessages, 1500);
				
	})

}
function afficherMessageDesAutres(listeMessages){
	var node = document.getElementById("tousLesMessages");
	var element;
	for (var i = 0; i<listeMessages.length; i++ ) {
		element = document.createElement("li");
		element.setAttribute("class", "messageDesAutres");

		//var str = "<";
		//element = str.replace("<", "&lt");
		//element = listeMessages[i].message.
		
		element.innerHTML = listeMessages[i].nomUsager + " : " + listeMessages[i].message ;
		node.appendChild(element);

	}
	//node.scrollTop = node.scrollHeight;

}


function envoyerMessage(){
	var message = document.getElementById('champMessage').value;
	document.getElementById('champMessage').value = "";

	envoyer(message);
}

function envoyer(message){
	$.ajax({
		type: 'POST', 					
		url: 'ajax.php',
		data: {				
			//nom du post: "valeur",
			champMessage : message
		}
	})
	.done(function(data) {
		data = JSON.parse(data); 
		afficherMessage(data);	
				
	})

}

function afficherMessage(message){
	var node = document.getElementById("tousLesMessages");
	var element;

	element = document.createElement("li");
	element.setAttribute("class", "message");
	element.innerHTML = message;
	node.appendChild(element);
}


function afficherMembres(listeMembres){
	var node = document.getElementById("listeUsers");
	var element;

	while (node.firstChild) {
	    node.removeChild(node.firstChild);
	}

	for (var membre in listeMembres) {
		element = document.createElement("li");
		//element.setAttribute("id", "membre");
		element.innerHTML = listeMembres[membre];
		node.appendChild(element);

	}

}


