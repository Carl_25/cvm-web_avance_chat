function Bonhomme(){
	this.compteur=0;
	this.etatSauter = false;
	this.velocityX=5;
	this.velocityY = 4;
	this.velocityFactorY = -0.6;

	this.x=50;
	this.y=365;

	this.node = document.createElement("div");
	this.node.setAttribute("class", "bonhomme");
	this.node.setAttribute("id","bonhomme");
	this.node.style.left = this.x + "px";
	this.node.style.top = this.y + "px";
	document.getElementById("jeuArea").appendChild(this.node);

}

Bonhomme.prototype.bougerGauche = function(){
	this.x -= this.velocityX;
	this.node.style.left = this.x + "px";
}

Bonhomme.prototype.bougerDroite = function(){
	this.x += this.velocityX;
	this.node.style.left = this.x + "px";
}

Bonhomme.prototype.tick = function(){
	
	if(this.etatSauter){
		if(this.compteur < 25){
			this.y -= this.velocityY;
		}
		if(this.compteur > 25 && this.compteur < 50){
			this.y += this.velocityY;
		}
		if(this.compteur >= 50){
			this.etatSauter = false;
			this.compteur = 0;
		}
		this.compteur++;

	}
	
	this.node.style.top = this.y + "px";
}

