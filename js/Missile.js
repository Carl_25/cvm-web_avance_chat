function Missile(x,y){
	this.x= x + 76;
	this.y= y + 30;
	this.velocityX = 10;
	//this.velocityFactorX = 0.6;

	this.node = document.createElement("div");
	this.node.setAttribute("class","missile");
	this.node.style.left = this.x + "px";
	this.node.style.top = this.y + "px";

	document.getElementById("jeuArea").appendChild(this.node);

}

Missile.prototype.tick = function(){
	//this.velocityX += this.velocityFactorX;
	this.x += this.velocityX;
	this.node.style.left=this.x + "px";

	//gestion des collisions avec le boss
	if(this.x == 346 && this.y <= 320){
		vieBoss--;
		console.log(vieBoss);

	}
	

	var alive = this.x < 680;

	if(!alive){
		this.node.parentNode.removeChild(this.node);
	}

	return this.x < 680;


}