function MissileEnnemi(){
	this.x= 600;
	this.y= Math.random() * 300;
	this.velocityX = 5;
	//this.velocityFactorX = 0.6;

	this.node = document.createElement("div");
	this.node.setAttribute("class","missileEnnemi");
	this.node.style.left = this.x + "px";
	this.node.style.top = this.y + "px";

	document.getElementById("jeuArea").appendChild(this.node);

}

MissileEnnemi.prototype.tick = function(){
	//this.velocityX += this.velocityFactorX;
	this.x -= this.velocityX;
	this.node.style.left=this.x + "px";

	//condition de défaite
	if(this.x >= spriteList[0].x && this.x <= spriteList[0].x + 75 && 
		this.y >= spriteList[0].y && this.y <= spriteList[0].y+96 ){

		mort = true;
		
	}

	var alive = this.x > 50;

	if(!alive){
		this.node.parentNode.removeChild(this.node);
	}

	//console.log("x du bonhoome : " + spriteList[0].x);
	//console.log("x du missie : " + this.x);

	return this.x > 50;


}