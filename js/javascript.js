spriteList = []
spriteListMissiles = []
spriteListMissilesEnnemi = []
var vieBoss=15;
var mort = false;

document.onkeypress = function(e){
	if(e.which == 32){

		gameInit();
		
	}

}

function gameInit(){
	
	spriteList.push(new Bonhomme() );
	tick();

	document.onkeypress = function(e){
		if(e.which == 97){
			spriteList[0].bougerGauche();
		}
		else if(e.which == 100){
			spriteList[0].bougerDroite();
		}
		else if(e.which == 119){ //pour sauter
			
			spriteList[0].etatSauter = true;
		}

		else if(e.which == 32){ //tirer un missile

			if(mort){
				//node = document.getElementById("jeuArea");
				//node.style.display = "block";

				node = document.getElementById("fin");
				node.style.display = "none";
			}
			else{
				spriteListMissiles.push(new Missile(spriteList[0].x, spriteList[0].y) );

			}
			
		}

	}


}

function tick(){
	for(var i = 0; i<spriteListMissiles.length;i++){

		var alive = spriteListMissiles[i].tick();

		if (!alive) {
			spriteListMissiles.splice(i, 1);
			i--;
		}

		
	}

	//créer des missies ennemis aléatoirement
	if(Math.random() * 100 > 92 ){
		spriteListMissilesEnnemi.push(new MissileEnnemi() );
	}

	//tick pour les missiles ennemis
	for(var i = 0; i<spriteListMissilesEnnemi.length;i++){

		var alive = spriteListMissilesEnnemi[i].tick();


		if (!alive) {
			spriteListMissilesEnnemi.splice(i, 1);
			i--;
		}

		
	}

	//pour faire sauter le bonhomme
	spriteList[0].tick();


	//condition de victoire
	if(vieBoss == 0){
		node = document.getElementById("jeuArea");
		node.style.display = "none";

		node = document.getElementById("win");
		node.style.display = "block";
	}

	//condition de défaite
	if(mort){

		node = document.getElementById("jeuArea");
		node.style.display = "none";

		node = document.getElementById("fin");
		node.style.display = "block";

	}
	
	


	setTimeout(tick,30);
}
