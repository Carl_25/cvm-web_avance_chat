$( document ).ready( tick );

i=0

var spriteList = [];

url1 = "images/bgSecretOfMana.jpg";
url2 = "images/sm1.png";
url3 = "images/sm2.jpg";
url4 = "images/sm3.jpg";
url5 = "images/sm4.jpg";
url6 = "images/sm5.png";

spriteList.push(url1 );
spriteList.push(url2 );
spriteList.push(url3 );
spriteList.push(url4 );
spriteList.push(url5 );
spriteList.push(url6 );

function tick(){

	document.documentElement.style.background = "url("+spriteList[i]+")";
	document.documentElement.style.backgroundSize = 'cover';
	document.documentElement.style.transition = "width 2s, height 2s, transform 2s";
	if(i == 5){
		i=0;
	}

	i++;
	setTimeout(tick, 4000);
}