<?php  
	require_once("action/indexAction.php");
	$action = new IndexAction();
	$action->execute();
	
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link href="css/global.css" rel="stylesheet" type="text/css" />
	<link id="cssTheme"  href="css/chat.css" rel="stylesheet" type="text/css" />
	<script src="js/changeCss.js"></script>

	<title>login</title>
</head>
<body>

<div class="loginDiv">

	<div class="loginFormDiv">

		<form action="index.php" method="post" accept-charset="utf-8">


			<div class="form-label">
				Nom usager:
			</div>
			<div class="form-input" on>
				<input type="text" name="usager">
			</div>
			<div class="form-clear"></div>

			<div class="form-label">
				Mot de passe:
			</div>
			<div class="form-input">
				<input type="password" name="mdp">
			</div>
			<div class="form-clear"></div>

			<div class="form-label">
				Thème:
			</div>

			<div class="form-input">
				<select onchange="changeFunc()" name="choixCss" id="selectCSS">
					<option value="chat">Contra</option>
					<option value="chatPhantasyStar">Phantasy Star</option>
					<option value="chatSecretOfMana">Secret Of mana</option>
				</select>

				
			</div>
			<button action="submit" name="boutonAuthentifier">Authentifier</button>
			<a href="inscription.php">s'inscrire</a>

			<div class="form-clear"></div>

			<?php  
				if($action->invalid){
					?>
					<div style="color:red;">Nom d'usager et mot de passe invalides</div>
					<?php

				}
			?>
		</form>
		
		

	</div>

</div>


</body>
</html>