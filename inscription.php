<?php  
	require_once("action/inscriptionAction.php");
	$action = new InscriptionAction(); 
	$action->execute();

	
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link href="css/global.css" rel="stylesheet" type="text/css" />
	<link href="css/chat.css" rel="stylesheet" type="text/css" />
	<title>login</title>
</head>
<body>
<?php  
	if($action->key){
		?>
		<div><?php echo $action->key ?> </div>

		<?php
	}
?>
<div class="loginDiv">
	<div class="loginFormDiv">
	
		<form action="inscription.php" method="post">
			<div class="form-label">
				Matricule:
			</div>
			<div class="form-input" on>
				<input type="text" name="matricule">
			</div>
			<div class="form-clear"></div>

			<div class="form-label">
				prenom:
			</div>
			<div class="form-input">
				<input type="text" name="prenom">
			</div>
			<div class="form-clear"></div>

			<div class="form-label">
				nom:
			</div>
			<div class="form-input">
				<input type="text" name="nom">
			</div>
			<div class="form-clear"></div>

			<div class="form-label">
				nom d'usager:
			</div>
			<div class="form-input">
				<input type="text" name="nomUsager">
			</div>
			<div class="form-clear"></div>

			<div class="form-label">
				Mot de passe:
			</div>
			<div class="form-input">
				<input type="password" name="mdp">
			</div>
			<div class="form-clear"></div>

			<div class="form-label">
				Message de bienvenue:
			</div>
			<div class="form-input">
				<input type="text" name="bienvenue">
			</div>
			<div class="form-clear"></div>

			
			<button action="submit" name="boutonInscription">s'inscrire</button>
			<a href="index.php">Connexion</a>

			<div class="form-clear"></div>

		</form>
		
		

	</div>

</div>


</body>
</html>